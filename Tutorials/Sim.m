clc; clear all; close all;

load('valuesA.mat');

subplot(3,1,1)
plot(tout, Acc.signals.values)
xlabel('Time [s]')
ylabel('Acceleration [m.s^{-2}]')

subplot(3,1,2)
plot(tout, Vel.signals.values*(3600/1000))
xlabel('Time [s]')
ylabel('Velocity [kph]')
subplot(3,1,3)

plot(tout, Pos.signals.values*(1/1000))
xlabel('Time [s]')
ylabel('Positon [km]')

