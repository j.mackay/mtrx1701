\documentclass[12pt,letterpaper]{article}
\usepackage{fullpage}
\usepackage[top=2cm, bottom=4.5cm, left=2.5cm, right=2.5cm]{geometry}
\usepackage{amsmath,amsthm,amsfonts,amssymb,amscd}
\usepackage{lastpage}
\usepackage{enumerate}
\usepackage{fancyhdr}
\usepackage{mathrsfs}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{hyperref}

\hypersetup{%
  colorlinks=true,
  linkcolor=blue,
  linkbordercolor={0 0 1}
}
 
\renewcommand\lstlistingname{Algorithm}
\renewcommand\lstlistlistingname{Algorithms}
\def\lstlistingautorefname{Alg.}

\lstdefinestyle{Python}{
    language        = Python,
    frame           = lines, 
    basicstyle      = \footnotesize,
    keywordstyle    = \color{blue},
    stringstyle     = \color{green},
    commentstyle    = \color{red}\ttfamily
}

\setlength{\parindent}{0.0in}
\setlength{\parskip}{0.05in}

% Edit these as appropriate
\newcommand\course{MTRX1701}
\newcommand\hwnumber{6}                  % <-- homework number
\newcommand\NetIDa{Jacob Mackay}           % <-- NetID of person #1
\newcommand\NetIDb{j.mackay@acfr.usyd.edu.au}           % <-- NetID of person #2 (Comment this line out for problem sets)

\pagestyle{fancyplain}
\headheight 35pt
\lhead{\NetIDa}
\lhead{\NetIDa\\\NetIDb}                 % <-- Comment this line out for problem sets (make sure you are person #1)
\chead{\textbf{\Large Tutorial \hwnumber}}
\rhead{\course \\ \today}
\lfoot{}
\cfoot{}
\rfoot{\small\thepage}
\headsep 1.5em

\begin{document}

\part*{Report Writing}

When presenting work that you have completed, it is important that you do so in a clear and concise manner with a logical and easy to follow format. This is a high priority in both industry and academia, so early mastery of this skill will help you immensely.

\section*{Abstract/Executive Summary}

Very short overview of subject matter, methods of analysis, findings, and recommendations (executive summary) or contributions/possibilities (abstract). 

\section*{Introduction}

Provide context and motivation for the report. Why subject matter is interesting/important, what has been done in the past, what challenges have been faced, a supposition of why they may have failed to meet certain criteria and what you're trying to achieve and how. Include an outline of the structure of the report.

\section*{Interesting Problem 1}

This is the beginning of 'the body'. Sometimes there may be multiple sections which address different problems contained in one report. For example, if your report is detailing an active perception loop (perception, representation, action), this section could address the perception part, and following sections could address representation, action, and finally fusion. It could also be part 1 of an assignment.

\subsection*{Introduction}

This section provides context and background to this specific section, and an outline of the section. Sometimes a heading is needed, but often a short paragraph or two following the section heading is sufficient.

\subsection*{Problem Setup/Analytical Formulation}

Here you would set up the design criteria for your problem, and the mathematical, algorithmic or other analytic tools that will be used. If appropriate, you can also introduce your system setup, and how that motivates or solves your problem. Diagrams, equations and algorithms should feature heavily here. An example can be seen in Figure \ref{fig:Bistatic}.

\begin{figure}[thpb]
      \centering
		\includegraphics[width=0.5\columnwidth]{bistatic3.eps}
      \caption{Simplified bistatic system setup. The receiver is located at the origin with bore-sight $\mathbf{b_R}$ pointing along the x-axis. The transmitter is at location $\mathbf{x_{RT}}$, with bore-sight $\mathbf{b_T}$. We are interested in integrating over the highlighted surface, enclosed by the transmitter and receiver beam-widths.}
      \label{fig:Bistatic}
\end{figure}

\subsection*{Problem Section 1}

Here you will show how you addressed the problem through experimentation and interpretation of the results. You would outline what you're presenting, how you collected/synthesised the data, a representation of the results, and an interpretation of what you found. Figures, photographs and tables should feature heavily here. It is very important to ensure that figures are appropriately labelled, captioned and titled (if the publication requires it). An example of a graph without a title can be seen in Figure \ref{fig:Antenna}. 

\begin{figure}[thpb]
      \centering
		\includegraphics[width=0.7\columnwidth]{antenna.eps}
      \caption{Measured and normalised antenna elevation gains. The radiation pattern function was fitted to the data using radial basis function kernels.}
      \label{fig:Antenna}
\end{figure}

\newpage

\subsection*{Problem Section 2}

Perhaps your sub-problem can be broken into further sections, one describing a unique system setup, and another the results from your data collection/simulation/... . You may use this section for presentation and analysis of results, similar to what is shown in Table \ref{table:Reflectivities}.

\begin{table}
\renewcommand{\arraystretch}{1.3}
\caption{Reflectivity Values of Targets in Road Scenes}
\label{table:Reflectivities}
\centering
\begin{tabular}{|l|l|l|l|l|} 
\hline
\textbf{Scene} & \textbf{Target} & \textbf{Reflectivity [dB]}  \\ \hline
1 & Concrete and Metal Manhole & 1.3  \\ \hline
1 & Metal Manhole & -0.9  \\ \hline
1 & Paintcan Lid (debris) & -2.9  \\ \hline
2 & Cracked Road & -11.1  \\ \hline
3 & Debris & -3.8  \\ \hline
3 & Plastic & -3.7  \\ \hline
3 & Pothole & 4.8  \\ \hline
4 & Cats Eyes & -7.9  \\ \hline
\end{tabular}
\end{table}

\subsubsection*{Small Part 1}

It might even be appropriate to break your report down further.

\subsubsection*{Small Part 2}

\subsection*{Conclusions}

Here you would consolidate your significant results from this section, present your interpretations within the original stated context and motivation. Drawing from results and context, you now present the implications of your findings and what should happen next.

\section*{Interesting Problem 2}

This is an new section, informed by the previous one, addressing a different part of the overall challenge.

\subsection*{Introduction}

\subsection*{Problem Setup/Analytical Formulation}

\subsection*{Problem Section 1}

\subsection*{Problem Section 2}

\subsection*{Conclusions}

\section*{Conclusions}

Similar to the sub-conclusions, but with grander scope and context, you include major results from all sections together, and the direction in which they point.

\section*{References}

Very important, IEEE style.

\section*{Appendix}

This section is for supplementary material which an interested reader can choose to read. This should not contain information required to read and interpret your report. For example you could include a mathematical derivation which is well known to some and is important for your report, but is not part of your contribution.
\\
\\
Additionally, you could attach code listings so that a reader could implement your system for verification. NOTE: This is not for important results or algorithms, simply supplementary material. An example listing is provided.

\lstinputlisting[language=Octave, firstline=1, lastline=18, frame=single]{Sim.m}


\section*{Assignments}
Work on Assignment 3. By the end of this lesson you should be into part 2! Enjoy!
   
\end{document}
